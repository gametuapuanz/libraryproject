/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import database.User;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author O-Live
 */
public class UserTableModel extends AbstractTableModel {

    ArrayList<User> userList = new ArrayList<User>();
    String[] columNames = {"#", "login name", "name", "surname", "type"};

    @Override

    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columindex) {
        User user = userList.get(rowIndex);
        switch (columindex) {
            case 0:
                return user.getUserId();
            case 1:
                return user.getLoginName();
            case 2:
                return user.getName();
            case 3:
                return user.getSurname();
            case 4:
                return user.getTypeId();
        }
        return "";
    }
    public void setData(ArrayList<User> userList){
    this.userList=userList;
    fireTableDataChanged();
    }

    @Override
    public String getColumnName(int colum) {
        return columNames[colum]; //To change body of generated methods, choose Tools | Templates.
    }

}
